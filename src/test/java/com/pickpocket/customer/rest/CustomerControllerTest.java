package com.pickpocket.customer.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.ws.rs.core.MediaType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pickpocket.customer.model.Customer;
import com.pickpocket.customer.service.CustomerService;
import com.pickpocket.customer.util.Util;

@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CustomerService customerService;

	@Autowired
	private ObjectMapper objectMapper;

	private Customer customer;

	@BeforeEach
	public void before() {
		customer = Util.populateCustomer();
	}

	@Test
	public void getCustomerByUserIdShouldReturnCustomer() throws Exception {

		when(customerService.getCustomer(Mockito.any())).thenReturn(customer);

		MvcResult result = this.mockMvc.perform(get("/get")).andDo(print()).andExpect(status().isOk()).andReturn();

		assertThat(result.getResponse().getContentAsString().contains(customer.getUserId()));
		assertThat(result.getResponse().getContentAsString().contains(customer.getUserName()));
		assertThat(result.getResponse().getContentAsString().contains(customer.getUserEmail()));
		assertThat(result.getResponse().getContentAsString().contains(customer.getUserAge().toString()));
	}

	@Test
	public void saveCustomerByUserIdShouldReturnUserId() throws Exception {
		when(customerService.saveCustomer(Mockito.any())).thenReturn(customer.getUserId());
		
		MvcResult result = this.mockMvc
				.perform(post("/save").contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(customer)))
				.andDo(print()).andExpect(status().isOk()).andReturn();
		assertThat(result.getResponse().getContentAsString().contains(customer.getUserId()));
	}

	@Test
	public void updateCustomerByUserIdShouldReturnUserId() throws Exception {
		when(customerService.updateCustomer(Mockito.any())).thenReturn(customer.getUserId());
		
		MvcResult result = this.mockMvc.perform(put("/update").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(customer))).andExpect(status().isOk()).andReturn();
		assertThat(result.getResponse().getContentAsString().contains(customer.getUserId()));
	}

	@Test
	public void deleteCustomerByUserIdShouldReturnUserId() throws Exception {
		when(customerService.deleteCustomer(Mockito.any())).thenReturn(customer.getUserId());
		
		MvcResult result = this.mockMvc.perform(delete("/delete")).andDo(print()).andExpect(status().isOk())
				.andReturn();
		assertThat(result.getResponse().getContentAsString().contains(customer.getUserId()));
	}

}
