package com.pickpocket.customer.doc;

import io.swagger.annotations.Contact;
import io.swagger.annotations.ExternalDocs;
import io.swagger.annotations.Info;
import io.swagger.annotations.License;
import io.swagger.annotations.SwaggerDefinition;

@SwaggerDefinition(info = @Info(description = "Pick-Pocket customer Resources", 
								version = "V1.0.0", 
								title = "Pick-Pocket customer Resource API", 
								contact = @Contact(name = "Ranga Karanam", email = "ranga@in28minutes.com", url = "http://www.in28minutes.com"), 
								license = @License(name = "Apache 2.0", url = "http://www.apache.org/licenses/LICENSE-2.0")), 
								consumes = {"application/json", "application/xml" }, 
								produces = { "application/json", "application/xml" }, 
								schemes = {SwaggerDefinition.Scheme.HTTP,SwaggerDefinition.Scheme.HTTPS }, 
								externalDocs = @ExternalDocs(value = "Read This For Sure", url = "http://in28minutes.com"))

public interface ApiDocumentationConfig {

}