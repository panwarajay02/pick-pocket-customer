package com.pickpocket.customer.model;

import java.time.LocalDateTime;

public class CustomerExceptionModel {

	private String code;
	private String message;
	private LocalDateTime timeStamp = LocalDateTime.now();

	public CustomerExceptionModel() {
		super();
	}

	public CustomerExceptionModel(String code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}

}
