package com.pickpocket.customer.model;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "All details about the Customer.")
public class Customer {

	@ApiModelProperty(name = "userId", notes = "Id of the customer")
	private String userId;

	@ApiModelProperty(name = "userName", notes = "Name of the customer")
	private String userName;

	@ApiModelProperty(name = "userEmail", notes = "Email of the customer")
	private String userEmail;

	@ApiModelProperty(name = "userAge", notes = "Age of the customer")
	private Integer userAge;

	@ApiModelProperty(name = "creationDate", notes = "Creation date of customer")
	private Date creationDate = new Date();

	@ApiModelProperty(name = "updationDate", notes = "Updation date of customer")
	private Date updationDate = new Date();

	public Customer() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public Integer getUserAge() {
		return userAge;
	}

	public void setUserAge(Integer userAge) {
		this.userAge = userAge;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdationDate() {
		return updationDate;
	}

	public void setUpdationDate(Date updationDate) {
		this.updationDate = updationDate;
	}

	@Override
	public String toString() {
		return "Customer [userId=" + userId + ", userName=" + userName + ", userEmail=" + userEmail + ", userAge="
				+ userAge + ", creationDate=" + creationDate + ", updationDate=" + updationDate + "]";
	}

}
