package com.pickpocket.customer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pickpocket.customer.model.Customer;
import com.pickpocket.customer.repo.CustomerRepository;
import com.pickpocket.customer.utils.Utility;

@Service
public class CustomerService {

	@Autowired
	CustomerRepository customerRepo;

	@Autowired
	Utility utility;

	@Autowired
	ObjectMapper objectMapper;

	public Customer getCustomer(String userId) {
		utility.checkValue(userId);
		com.pickpocket.customer.mongo.Customer dbCustomer = customerRepo.findCustomerByUserId(userId);
		return objectMapper.convertValue(dbCustomer, Customer.class);
	}

	public String saveCustomer(Customer customer) {
		utility.checkValue(customer.getUserName());
		com.pickpocket.customer.mongo.Customer dbCustomer = objectMapper.convertValue(customer,
				com.pickpocket.customer.mongo.Customer.class);
		com.pickpocket.customer.mongo.Customer dbResponse = customerRepo.insert(dbCustomer);
		return dbResponse.getUserId();
	}

	public String updateCustomer(Customer customer) {
		utility.checkValue(customer.getUserName());
		com.pickpocket.customer.mongo.Customer dbCustomer = objectMapper.convertValue(customer,
				com.pickpocket.customer.mongo.Customer.class);
		com.pickpocket.customer.mongo.Customer dbResponse = customerRepo.save(dbCustomer);
		return dbResponse.getUserId();
	}

	public String deleteCustomer(String userId) {
		utility.checkValue(userId);
		customerRepo.deleteCustomerByUserId(userId);
		return userId;
	}

}
