package com.pickpocket.customer.aop;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggingAspect {

	private final Logger log = LoggerFactory.getLogger(LoggingAspect.class);

	@Before(value = "execution(* com.pickpocket.customer..*(..)))")
	public void before(JoinPoint joinPoint) {

		if (Objects.isNull(joinPoint.getArgs()) || joinPoint.getArgs().length == 0) {
			log.info("{}.{} : Init ", joinPoint.getSignature().getDeclaringTypeName(),
					joinPoint.getSignature().getName());
		} else {
			CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();
			Map<Object, Object> map = new HashMap<>();

			IntStream.range(0, joinPoint.getArgs().length - 1).forEach(ind -> {
				map.put(codeSignature.getParameterNames()[ind], joinPoint.getArgs()[ind]);
			});
			log.info("{}.{} : Init with params: {}", joinPoint.getSignature().getDeclaringTypeName(),
					joinPoint.getSignature().getName(), map);
		}
	}

	@AfterThrowing(value = "execution(* com.pickpocket.customer..*(..)))", throwing = "exception")
	public void afterThrowing(JoinPoint joinPoint, Throwable exception) {
		log.info("{}.{} : Exception with message: {}", joinPoint.getSignature().getDeclaringTypeName(),
				joinPoint.getSignature().getName(), exception.getMessage());
	}

	@AfterReturning(value = "execution(* com.pickpocket.customer..*(..)))", returning = "result")
	public void afterReturning(JoinPoint joinPoint, Object result) {
		if (Objects.isNull(result)) {
			log.info("{}.{} : Exit.", joinPoint.getSignature().getDeclaringTypeName(),
					joinPoint.getSignature().getName());
		} else {
			log.info("{}.{} : Exit with value: {}", joinPoint.getSignature().getDeclaringTypeName(),
					joinPoint.getSignature().getName(), result);
		}
	}

}
