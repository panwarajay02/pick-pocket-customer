package com.pickpocket.customer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.pickpocket.customer.model.CustomerExceptionModel;
import com.pickpocket.customer.utils.CustomerConstants;

@ControllerAdvice
public class CustomerExceptionHandler {

	@ResponseBody
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = CustomerException.class)
	public CustomerExceptionModel exception(CustomerException exception) {
		return new CustomerExceptionModel(exception.getCode(), exception.getMessage());
	}

	@ResponseBody
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(value = RuntimeException.class)
	public CustomerExceptionModel runtimeException(Exception exception) {
		return new CustomerExceptionModel(CustomerConstants.GENERIC_ERROR_CODE,
				CustomerConstants.GENERIC_ERROR_CODE_MESSAGE + exception.getMessage());
	}

}
