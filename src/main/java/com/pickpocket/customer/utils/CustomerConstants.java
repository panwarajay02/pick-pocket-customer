package com.pickpocket.customer.utils;

public class CustomerConstants {

	private CustomerConstants() {
	}

	public static final String GENERIC_ERROR_CODE = "5001";
	public static final String GENERIC_ERROR_CODE_MESSAGE = "Generic error.";

	public static final String USERNAME_NOT_FOUND_CODE = "4001";
	public static final String USERNAME_NOT_FOUND_MESSAGE = "Username is null or empty.";
	
}
