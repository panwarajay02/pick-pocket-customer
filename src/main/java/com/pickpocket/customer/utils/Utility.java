package com.pickpocket.customer.utils;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.pickpocket.customer.exception.CustomerException;

@Component
public class Utility {

	public void checkValue(String value){
		if (StringUtils.isEmpty(value)) {
			throw new CustomerException(CustomerConstants.USERNAME_NOT_FOUND_CODE, CustomerConstants.USERNAME_NOT_FOUND_MESSAGE);
		}
	}
}
