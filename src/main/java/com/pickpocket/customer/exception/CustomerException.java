package com.pickpocket.customer.exception;

import java.time.LocalDateTime;

public class CustomerException extends RuntimeException {

	private static final long serialVersionUID = -915960560517810089L;
	private String code;
	private String message;
	private LocalDateTime timestamp;

	public CustomerException() {
		super();
		this.timestamp = LocalDateTime.now();
	}

	public CustomerException(String code, String message) {
		this.code = code;
		this.message = message;
		this.timestamp = LocalDateTime.now();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

}
