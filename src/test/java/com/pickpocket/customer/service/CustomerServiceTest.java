package com.pickpocket.customer.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pickpocket.customer.model.Customer;
import com.pickpocket.customer.repo.CustomerRepository;
import com.pickpocket.customer.util.Util;
import com.pickpocket.customer.utils.Utility;

@SpringBootTest
public class CustomerServiceTest {

	@Mock
	CustomerRepository customerRepo;

	@Mock
	Utility utility;

	@Mock
	ObjectMapper objectMapper;

	@InjectMocks
	CustomerService customerService;

	private Customer customer;

	@BeforeEach
	public void beforeEach() {
		customer = Util.populateCustomer();
		Mockito.when(objectMapper.convertValue(Mockito.any(), Mockito.eq(Customer.class))).thenReturn(customer);
		Mockito.when(objectMapper.convertValue(Mockito.any(), Mockito.eq(com.pickpocket.customer.mongo.Customer.class))).thenReturn(Util.populateMongoDBCustomer());
		Mockito.doNothing().when(utility).checkValue(Mockito.any());
	}

	@Test
	public void testGetCustomerShouldReturnCustomer() {
		Mockito.when(customerRepo.findCustomerByUserId(Mockito.any())).thenReturn(Util.populateMongoDBCustomer());
		Customer response = customerService.getCustomer("12345");
		assertThat(response.getUserId().equals(customer.getUserId()));
	}

	@Test
	public void testSaveCustomerShouldReturnUserId() {
		Mockito.when(customerRepo.insert(Mockito.any(com.pickpocket.customer.mongo.Customer.class))).thenReturn(Util.populateMongoDBCustomer());
		String userId = customerService.saveCustomer(customer);
		assertThat(userId.equals(customer.getUserId()));
	}

	@Test
	public void testpPdateCustomerShouldReturnUserId() {
		Mockito.when(customerRepo.save(Mockito.any(com.pickpocket.customer.mongo.Customer.class))).thenReturn(Util.populateMongoDBCustomer());
		String userId = customerService.updateCustomer(customer);
		assertThat(userId.equals(customer.getUserId()));
	}

	@Test
	public void testDeleteCustomerShouldReturnCustomer() {
		Mockito.when(customerRepo.deleteCustomerByUserId(Mockito.any())).thenReturn(Util.populateMongoDBCustomer());
		String userId = customerService.deleteCustomer(customer.getUserId());
		assertThat(userId.equals(customer.getUserId()));
	}

}
