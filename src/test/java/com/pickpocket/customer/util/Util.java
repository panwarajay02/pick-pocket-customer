package com.pickpocket.customer.util;

import java.util.Date;

import com.pickpocket.customer.model.Customer;

public class Util {

	public static Customer populateCustomer() {
		Customer customer = new Customer();
		customer.setUserId("1234");
		customer.setUserName("username");
		customer.setUserAge(24);
		customer.setUserEmail("test@test.test");
		customer.setCreationDate(new Date());
		customer.setUpdationDate(new Date());
		return customer;
	}

	public static com.pickpocket.customer.mongo.Customer populateMongoDBCustomer() {
		com.pickpocket.customer.mongo.Customer customer = new com.pickpocket.customer.mongo.Customer();
		customer.setUserId("1234");
		customer.setUserName("username");
		customer.setUserAge(24);
		customer.setUserEmail("test@test.test");
		customer.setCreationDate(new Date());
		customer.setUpdationDate(new Date());
		return customer;
	}

}
