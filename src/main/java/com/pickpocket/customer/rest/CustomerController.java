package com.pickpocket.customer.rest;

import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.pickpocket.customer.model.Customer;
import com.pickpocket.customer.service.CustomerService;

import io.swagger.annotations.ApiOperation;

@RestController
public class CustomerController {

	@Autowired
	private CustomerService customerService;

	@ResponseBody
	@GetMapping(path = "/get")
	@ApiOperation(value = "Returns customer by userId", notes = "Returns all customer information.")
	public Customer getCustomerName(@QueryParam(value = "userId") String userId) {
		return customerService.getCustomer(userId);
	}

	@ResponseBody
	@PostMapping(path = "/save")
	@ApiOperation(value = "Inserts a new customer", notes = "Takes Customer as input and inserts a new record.")
	public String saveCustomerName(@RequestBody Customer customer) {
		return customerService.saveCustomer(customer);
	}

	@ResponseBody
	@PutMapping(path = "/update")
	@ApiOperation(value = "Updates a customer", notes = "Takes Customer as input and updates it.")
	public String updateCustomerName(@RequestBody Customer customer) {
		return customerService.updateCustomer(customer);
	}

	@ResponseBody
	@DeleteMapping(path = "/delete")
	@ApiOperation(value = "Deletes a customer", notes = "Deletes a customer by its userId.")
	public String deleteCustomerName(@QueryParam(value = "userId") String userId) {
		return customerService.deleteCustomer(userId);
	}

}
