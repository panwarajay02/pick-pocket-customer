package com.pickpocket.customer.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.pickpocket.customer.mongo.Customer;

public interface CustomerRepository extends MongoRepository<Customer, String> {

	public Customer findCustomerByUserId(String userId);

	public Customer deleteCustomerByUserId(String userId);

}
